import java.util.Arrays;
import java.util.Objects;

public class TaskManagerTest {
    private TaskManager taskManager;
    public TaskManagerTest()
    {
        taskManager = new TaskManager();
    }

    public boolean Validate()
    {
        int[][] inputOne = new int[][] {
                new int[] {3, 3},
                new int[] {3, 10},
                new int[] {2, 5},
                new int[] {4, 5},
        };
        int[] outputOne = taskManager.GetTaskOrder(inputOne);
        boolean resultOne = Arrays.equals(new int[] {9, 4, 9}, outputOne);

        int[][] inputTwo = new int[][] {
                new int[] {3, 1},
                new int[] {12, 4},
                new int[] {11, 4},
                new int[] {10, 4},
        };
        int[] outputTwo = taskManager.GetTaskOrder(inputTwo);
        boolean resultTwo = Arrays.equals(new int[] {22, 18, 14}, outputTwo);

        return resultOne && resultTwo;
    }
}
