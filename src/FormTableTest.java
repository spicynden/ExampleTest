import java.util.Objects;

public class FormTableTest {
    private FormTable formTable;

    public FormTableTest()
    {
        formTable = new FormTable();
    }

    public boolean Validate()
    {
        String input = "|test|me|one two|\n" +
                       "|    a  | three   spaces |  |";

        String output = "|test|     me     |one two|\n" +
                        "| a  |three spaces|       |\n";

        return Objects.equals(formTable.Formated(input), output);
    }
}
