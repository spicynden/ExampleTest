import java.security.SecureRandom;
import java.util.Objects;
import java.lang.Exception;

public class FormTable {
    String input = "|test|me|one two|\n" +
            "|    a  | three   spaces |  |";

    public String Formated(String input)
    {
        String[][] table = InitTable(input);
        table = UpdateTable(table);
        return GenerateString(table);
    }

    private String[][] InitTable(String input)
    {
        if (Validate(input)) {
            String[] rows = input.split("\n");
            String[] columnsOneRow = rows[0].split("\\|");

            String[][] table = new String[rows.length][columnsOneRow.length - 1];

            for (int i = 0; i < rows.length; i++) {
                String[] columns = rows[i].split("\\|");
                for (int u = 0; u < columns.length - 1; u++) {
                    table[i][u] = columns[u + 1];
                }
            }

            return table;
        }
        throw new java.lang.Error("Входная строка не соответствует критериям");
    }

    private boolean Validate(String input)
    {
        String[] rows = input.split("\n");
        String[] columnsOneRow = rows.length > 0 ? rows[0].split("\\|") : new String[0];
        return rows.length > 0 && columnsOneRow.length > 0;
    }

    private String[][] UpdateTable(String[][] table)
    {
        // Убрать пробелы
        for (int i = 0; i < table.length; i++) {
            for (int u = 0; u < table[i].length; u++) {
                String[] cells = table[i][u].split(" ");
                int countWords = 0;
                for (String s : cells) {
                    if (s.length() > 0)
                        countWords ++;
                }
                String[] words = new String[countWords];
                int InxWords = 0;
                for (String s : cells) {
                    if (s.length() > 0) {
                        words[InxWords] = s;
                        InxWords += 1;
                    }
                }
                StringBuilder cellInput = new StringBuilder();
                for (int x = 0; x < words.length; x++) {
                    if (x == 0)
                        cellInput.append(words[x]);
                    else
                        cellInput.append(" " + words[x]);
                }
                table[i][u] = cellInput.toString();
            }
        }

        // найти ширину
        int[] columnLength = new int[table[0].length];
        for (String[] row : table) {
            for (int u = 0; u < row.length; u++) {
                int lengthCell = row[u].length();
                columnLength[u] = Math.max(columnLength[u], lengthCell);
            }
        }

        // установить ширину и центровать
        for (int i = 0; i < table.length; i++) {
            for (int u = 0; u < table[i].length; u++) {
                if (table[i][u].length() < columnLength[u])
                {
                    int difference = columnLength[u] - table[i][u].length();
                    int leftTabs = (int)Math.ceil(difference / 2);
                    int rightTabs = difference - leftTabs;

                    int leftIndx = 0;
                    while (leftIndx < leftTabs)
                    {
                        table[i][u] = " " + table[i][u];
                        leftIndx += 1;
                    }

                    int rightIndx = 0;
                    while (rightIndx < rightTabs)
                    {
                        table[i][u] = table[i][u] + " ";
                        rightIndx += 1;
                    }
                }
            }
        }

        return table;
    }

    private String GenerateString (String[][] table)
    {
        String output = "";

        for (int i = 0; i < table.length; i++) {
            output += "|";
            for (int z = 0; z < table[i].length; z++) {
                output += table[i][z];
                if (z != table[i].length - 1)
                    output += "|";
            }
            output += "|\n";
        }

        return output;
    }
}
