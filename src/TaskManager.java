import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

public class TaskManager {
    public int[] GetTaskOrder(int[][] input)
    {
        int throughput = input[0][1];
        int[][] tasks = new int[input.length - 1][2];
        System.arraycopy(input, 1, tasks, 0, input.length - 1);

        return CheckTimeCancelTask(throughput, tasks, new ArrayList<int[]>(), new ArrayList<int[]>(), 1);
    }

    public int[] CheckTimeCancelTask(int throughput, int[][] tasks, List<int[]> activeTask, List<int[]> canceledTask, int time)
    {
        if (canceledTask.size() == tasks.length) {
            return canceledTask.stream()
                    .sorted(Comparator.comparing(e -> e[0]))
                    .mapToInt(e -> e[3])
                    .toArray();
        }

        // activeTask 0 - индекс в исходном массиве 1 - время старта 2 - размер задания
        // добавим новое активное задание
        if (Arrays.stream(tasks).anyMatch(e -> e[0] == time)) {
            int[] toStartTask = IntStream.range(0, tasks.length)
                    .filter(i -> tasks[i][0] == time)
                    .mapToObj(i -> new int[] {i,  tasks[i][0], tasks[i][1]})
                    .findFirst().get();
            activeTask.add(toStartTask);
        }

        // установим приоритет загрузки заданий
        List<int[]> activeTaskPriority = new ArrayList<>();
        int priorityIndx = 0;
        for(int[] task :activeTask.stream().sorted(Comparator.comparing(e -> e[1])).toArray(int[][]::new))
        {
            priorityIndx += 1;
            activeTaskPriority.add(new int[] {task[1], priorityIndx});
        }

        int residualMemory = throughput;
        // распределим пропускную способность
        for (int i = 0; i < activeTask.size(); i++)
        {
            int [] task = activeTask.get(i);
            int startTimeTask = task[1];
            int priority = activeTaskPriority.stream().filter(e -> e[0] == startTimeTask).findFirst().get()[1];
            double unit = (double)throughput / (double)activeTask.size();
            int unitDwn = 0;
            if (priority <= Math.ceil((double)activeTask.size() / 2.00) && residualMemory > 0) {
                unitDwn = (int)Math.ceil(unit);
                residualMemory -= (int)Math.ceil(unit);
            }
            else if (residualMemory > 0) {
                unitDwn = (int)Math.floor(unit);
                residualMemory -= (int)Math.floor(unit);
            }

            int[] updatedTask = new int[] { task[0], task[1], task[2] - unitDwn };
            activeTask.set(i, updatedTask);
        }

        activeTask.stream().filter(e -> e[2] <= 0).forEach(e -> canceledTask.add(new int[] {e[0], e[1], e[2], time + 1}));
        activeTask.removeIf(e -> e[2] <=0);

        return CheckTimeCancelTask(throughput, tasks, activeTask, canceledTask, time + 1);
    }
}
