import java.util.Scanner;
import java.util.Arrays;
public class Main {
    public static void main(String[] args) {

        FormTableTest tableTest = new FormTableTest();
        System.out.println("tableTest: " + tableTest.Validate());

        PyramideTest pyramideTest = new PyramideTest();
        System.out.println("pyramideTest: " + pyramideTest.Validate());

        TaskManagerTest taskManagerTest = new TaskManagerTest();
        System.out.println("taskManagerTest: "+ taskManagerTest.Validate());
    }
}