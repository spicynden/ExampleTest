import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
public class Pyramide {
    public int MaxHeightPyramide(int[] blocks)
    {
        Arrays.sort(blocks);
        return CheckMaxHeightPyramide(blocks,1, 0, 0);
    }

    private int CheckMaxHeightPyramide(int[] blocks, int indx, int predIndx, int count)
    {
        if (indx < blocks.length) {
            if (blocks[indx] > blocks[predIndx]) {
                if (indx == 1)
                    count += 1;
                count += 1;
                return CheckMaxHeightPyramide(blocks, indx + 1, predIndx + 1, count);
            } else if (blocks[indx] == blocks[predIndx]) {
                int countEq = CountEquals(blocks, indx);
                count += CheckCountBeforeCountEq(blocks, indx, countEq);
                return CheckMaxHeightPyramide(blocks, indx + countEq, predIndx + countEq, count);
            }
        }
        return count;
    }

    private int CountEquals(int[] blocks, int indx)
    {
        return(int)Arrays.stream(blocks).filter(e -> e == blocks[indx]).count();
    }

    private int CheckCountBeforeCountEq (int[] blocks, int indx, int countEq)
    {
        if (indx - 2 >= 0)
        {
            int predCountEq = CountEquals(blocks, indx - 2);

            if (blocks[indx] - blocks[indx - 2] == 1)
                return predCountEq > 1 ? 1 : 2;

            if(blocks[indx] - blocks[indx - 2] > 1)
            {
                return countEq >= 3 ? blocks[indx] > 1 ? 3 : 2 : 2;
            }
        }
        return countEq >= 3 ? blocks[indx] > 1 ? 3 : 2 : 2;
    }

}
