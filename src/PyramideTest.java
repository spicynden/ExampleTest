import java.util.Objects;

public class PyramideTest {
    private Pyramide pyramide;
    public PyramideTest()
    {
        pyramide = new Pyramide();
    }

    public boolean Validate()
    {
        int[] inputOne = new int[] {3, 2, 4, 1};
        int outputOne = pyramide.MaxHeightPyramide(inputOne);
        boolean resultOne = outputOne == 4;
        
        int[] inputTwo = new int[] {1, 1, 1, 4, 4, 4};
        int outputTwo = pyramide.MaxHeightPyramide(inputTwo);
        boolean resultTwo = outputTwo == 5;

        return resultOne && resultTwo;
    }
}
